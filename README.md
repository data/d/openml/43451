# OpenML dataset: Peak-Detection-Dataset

https://www.openml.org/d/43451

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
I am currently writing a seminar paper about content summarization in Twitter networks. One of the Frameworks I read about uses a peak detection algorithm to cluster tweets by topic-aware peak times. I started wondering if there are any interesting peak detection approaches. Feel free to play around with the data and provide your peak detection algorithms! They don't necessarily have to be machine learning algorithms.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43451) of an [OpenML dataset](https://www.openml.org/d/43451). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43451/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43451/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43451/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

